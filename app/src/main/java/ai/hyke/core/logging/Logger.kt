package ai.hyke.core.logging

import android.util.Log

object Logger {

    private val debuggable = BuildConfig.DEBUG

    fun e(tag: String, msg: String) {
        if (debuggable)

            Log.e(tag, msg)
    }

    fun e(tag: String, msg: String?, e: Exception) {
        if (debuggable)
            Log.e(tag, msg, e)
    }

    fun d(tag: String, msg: String) {
        if (debuggable)
            Log.d(tag, msg)
    }

    fun i(tag: String, msg: String) {
        if (debuggable)
            Log.i(tag, msg)
    }

    fun w(tag: String, msg: String) {
        if (debuggable)
            Log.i(tag, msg)
    }

    fun printStackTrace(throwable: Throwable) {
        if (debuggable)
            throwable.printStackTrace()
    }
}
